package org.example;

import org.example.dao.BookImpl;
import org.example.service.BookService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        BookService service = new BookService(new BookImpl());

        System.out.println(service.getBookById(1));

    }
}
