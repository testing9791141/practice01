package org.example.service;

import org.assertj.core.api.Assertions;
import org.example.dao.BookDao;
import org.example.model.Book;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

//@ExtendWith(MockitoEx)
class BookServiceTest {

    @Mock
    BookDao bookDaoMock;

    @InjectMocks
    BookService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getBookByIdOldStyle() {
        BookDao bookDaoMock = Mockito.mock(BookDao.class);
        BookService service = new BookService(bookDaoMock);

        Book book = new Book(1, "test book1", 1.0);
        Mockito.when(bookDaoMock.getBookById(Mockito.anyInt())).thenReturn(book);

        Assertions.assertThat(service.getBookById(1).getName()).isEqualTo("test book1");
    }

    @Test
    void getBookById() {
        Book book = new Book(1, "test book1", 1.0);
        Mockito.when(bookDaoMock.getBookById(Mockito.anyInt())).thenReturn(book);

        Assertions.assertThat(service.getBookById(1).getName()).isEqualTo("test book1");
    }

    @Test
    void getAllBooksInStock() {
    }

    @Test
    void addToStock() {
    }

    @Test
    void removeFromStock() {
    }

    @Test
    void getPriceWithDiscount() {
    }
}